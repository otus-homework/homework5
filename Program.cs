﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Homework5
{
    class Program
    {
        static void Main(string[] args)
        {
            var htmlCode = GetHtmlCode("https://www.wakanim.tv/ru/v2");
            FindUrlFromHtmlCode(htmlCode);

            Console.ReadKey();
        }

        /// <summary>
        /// Получение html разметки по url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private static string GetHtmlCode(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            StreamReader sr = new StreamReader(response.GetResponseStream());
            var result = sr.ReadToEnd();
            sr.Close();

            return result;
        }

        /// <summary>
        /// Поиск url в html
        /// </summary>
        /// <param name="htmlCode"></param>
        private static void FindUrlFromHtmlCode(string htmlCode)
        {
            string pattern = @"((https?:\/\/(www\.)?)([\w\-\.\/])*)";

            var m = Regex.Match(htmlCode, pattern,
                        RegexOptions.IgnoreCase | RegexOptions.Compiled,
                        TimeSpan.FromSeconds(1));

            while (m.Success)
            {
                Console.WriteLine("Found url " + m.Groups[1] + " at "
                   + m.Groups[1].Index);
                m = m.NextMatch();
            }
        }
    }
}
